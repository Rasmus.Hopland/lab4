package datastructure;

import java.util.NoSuchElementException;

import cellular.CellState;

public class CellGrid implements IGrid {
    //Rows in grid.
    private int rows;
    //Columns in grid.
    private int columns;
    //GridState
    private CellState gridState[][];

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.gridState = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                gridState[row][col] = initialState;
            }
        }
	}
    
/*
    //Grid baseline.
    private int[][] grid;
    //Grid coordinate to hashmap value
    HashMap<Coordinate, CellState> gridState;
    //Coordinate container
    Coordinate coord = new Coordinate(); 

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.grid = new int[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                coord.setCoord(row,col);
                System.out.println(coord.getX());
            }
        }
	}*/

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        try {
            gridState[row][column] = element;
        } catch(IllegalArgumentException e) {
            throw new IllegalArgumentException();
        } catch(NoSuchElementException e) {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        try {
            return gridState[row][column];
        } catch(IllegalArgumentException e) {
            throw new IllegalArgumentException();
        } catch(NoSuchElementException e) {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public IGrid copy() {
        int x = numRows();
        int y = numColumns();
        IGrid newGrid = new CellGrid(x, y, CellState.DEAD);
        //CellState newGridState[][] = new CellState[x][y];
        for (int row = 0; row < x; row++) {
            for (int col = 0; col < y; col++) {
                CellState input = gridState[row][col];
                newGrid.set(row, col, input);
            }
        }
        return newGrid;
    }
    
}
