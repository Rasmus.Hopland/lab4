package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid brainGrid;

    public BriansBrain(int x, int y) {
        brainGrid = new CellGrid(x, y, CellState.DEAD);
        initializeCells();
    }

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < brainGrid.numRows(); row++) {
			for (int col = 0; col < brainGrid.numColumns(); col++) {
				if (random.nextBoolean()) {
					brainGrid.set(row, col, CellState.ALIVE);
				} else {
					brainGrid.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public CellState getCellState(int row, int column) {
        return brainGrid.get(row, column);
    }

    @Override
    public void step() {
        IGrid nextGeneration = brainGrid.copy();
		//Setting new Generation to their new state
		for (int i = 0; i < brainGrid.numRows(); i++) {
			for (int j = 0; j < brainGrid.numColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		//Setting new generation as current one.
		brainGrid = nextGeneration;
	}

    @Override
    public CellState getNextCell(int row, int col) {
        CellState newState;
		int alive = this.countNeighbors(row, col, CellState.ALIVE);
		CellState currentCell =  getCellState(row, col);
		if (currentCell.equals(CellState.DEAD)) {
			if (alive == 2) {
				newState = CellState.ALIVE;
			} else {
				newState = CellState.DEAD;
			}
		} else if (currentCell.equals(CellState.ALIVE)) {
			newState = CellState.DYING;
		} else if (currentCell.equals(CellState.DYING)) {
            newState = CellState.DEAD;
        } else {
			System.out.println("Something went wrong, no cellState");
			newState = null;
		}
		return newState;
	}
    private int countNeighbors(int row, int col, CellState state) {
		int alive = 0;
		for (int i = row-1; i <= row+1; i++) {
			for (int j = col-1; j <= col+1; j++) {
				try {
					if (brainGrid.get(i, j).equals(state) && !(i == row && j == col)) {
						alive++;
					}
				} catch(IndexOutOfBoundsException e) {
					continue;
				}
				
			}
		}
		return alive;
	}

    @Override
    public int numberOfRows() {
        return brainGrid.numRows();
    }

    @Override
    public int numberOfColumns() {
        return brainGrid.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return brainGrid;
    }
    
}
